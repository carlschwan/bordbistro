/**
 * SPDX-FileCopyrightText: 2019 Nicolas Fella <nicolas.fella@gmx.de>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

#include "articlesmodel.h"

#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QNetworkAccessManager>
#include <QNetworkReply>

ArticlesModel::ArticlesModel(QObject *parent, bool useTestData)
    : QAbstractListModel(parent)
    , m_useTestData(useTestData)
{
    if (useTestData) {
        QFile articlesFile(":/testdata/articles.json");
        articlesFile.open(QIODevice::ReadOnly);

        loadData(articlesFile.readAll());
    } else {
        QNetworkAccessManager *nam = new QNetworkAccessManager;

        connect(nam, &QNetworkAccessManager::finished, this, [this](QNetworkReply *reply) {
            if (reply->error()) {
                qDebug() << "error fetching articles" << reply->errorString();
                reply->deleteLater();
                return;
            }

            const QByteArray data = reply->readAll();

            loadData(data);
            reply->deleteLater();
        });

        nam->get(QNetworkRequest(QUrl(m_urlBase + "/bap/api/articles")));
    }
}

void ArticlesModel::loadData(const QByteArray &data)
{
    const QJsonDocument doc = QJsonDocument::fromJson(data);

    const QJsonArray articles = doc.array();

    beginResetModel();

    for (const QJsonValue &val : articles) {
        const QJsonObject obj = val.toObject();
        Article a;

        a.title = obj["title"].toString();
        a.category = obj["category"].toString();

        QStringList priceStrings;

        const QJsonArray options = obj["options"].toArray();
        for (const QJsonValue optionVal : options) {
            const QJsonObject option = optionVal.toObject();

            if (option["name"].toString() == "default") {
                priceStrings << QLocale::system().toCurrencyString(option["price"].toDouble(), "€");
            } else {
                priceStrings << option["name"].toString() + ": " + QLocale::system().toCurrencyString(option["price"].toDouble(), "€");
            }
        }

        a.price = priceStrings.join(" • ");

        const QString imageUrl = obj["imageUrl"].toString();

        QString imageFile;

        if (m_useTestData) {
            imageFile = "qrc:/testdata/images/" + imageUrl.mid(imageUrl.lastIndexOf('/') + 1);
        } else {
            imageFile = m_urlBase + "/" + imageUrl;
        }
        a.imageFile = imageFile;

        a.available = obj["available"].toBool();

        const QJsonArray declarations = obj["declarations"].toArray();
        for (const QJsonValue &declValue : declarations) {
            const QJsonObject declObj = declValue.toObject();
            a.declarations << declObj["description"].toString();
        }

        m_articles << a;
    }

    endResetModel();
}

int ArticlesModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_articles.length();
}

QVariant ArticlesModel::data(const QModelIndex &index, int role) const
{
    int row = index.row();

    switch (static_cast<Roles>(role)) {
    case TitleRole:
        return m_articles[row].title;
    case CategoryRole:
        return m_articles[row].category;
    case PriceRole:
        return m_articles[row].price;
    case ImageFileRole:
        return m_articles[row].imageFile;
    case AvailableRole:
        return m_articles[row].available;
    case DeclarationsRole:
        return m_articles[row].declarations;
    }

    return m_articles[index.row()].title;
}

QHash<int, QByteArray> ArticlesModel::roleNames() const
{
    return {
        {Roles::TitleRole, "title"},
        {Roles::CategoryRole, "category"},
        {Roles::PriceRole, "price"},
        {Roles::ImageFileRole, "imageFile"},
        {Roles::AvailableRole, "available"},
        {Roles::DeclarationsRole, "declarations"},
    };
}
