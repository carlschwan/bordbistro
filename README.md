# Summary

This is an app that fetches the bord bisto menu in German ICE trains and displays it.

# Building

This app depends on:
 - Qt5
 - Extra CMake Modules
 - Kirigami
 - KItemModels

Building works as for any CMake project:

```
cmake -B build .
cd build
make
```

Then run `./bordbistro`

# Data

The app fetches its data from the onboard API that is available when connected to the Wifi.

For testing/development purposes it contains a dump of the API data. Run the app with `--testing` to use that.

# Todo

Several things are missing:
 - Properly handle network errors
 - Indicate bistro availability
 - Indicate when API is not available
