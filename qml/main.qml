/**
 * SPDX-FileCopyrightText: 2019 Nicolas Fella <nicolas.fella@gmx.de>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

import QtQuick 2.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.15 as Kirigami
import org.kde.kitemmodels 1.0 as Models

import bap 1.0

Kirigami.ApplicationWindow {

    width: 800
    height: 800

    pageStack.initialPage: Kirigami.ScrollablePage {

        title: "Bordbistro"

        ListView {

            model: Models.KSortFilterProxyModel {
                sourceModel: ArticlesModel
                sortRole: "category"
            }

            delegate: ArticleDelegate {
                width: ListView.view.width
            }

            section {
                property: "category"
                delegate: Kirigami.ListSectionHeader {
                    label: section
                }
            }
        }
    }
}
